package base;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	WebDriver driver;

	
	public WebDriver launchBrowser(String url) throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "F:\\AutomationWithMohan\\chromedriver_win32 (10)\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(url);
		Thread.sleep(5000);
		return driver;
	}
	
	public void openNewBrowserForOTP()
	{
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String>s= driver.getWindowHandles();
		ArrayList <String>tabs= new ArrayList<String>(s);
		driver.switchTo().window(tabs.get(1));
		driver.get("http://smsapp.alertsindia.in/files/");
	}

}
