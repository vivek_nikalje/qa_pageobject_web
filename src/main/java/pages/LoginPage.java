package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class LoginPage  {
	
	WebDriver driver;
	//WebElement username=driver.findElement(By.id("mobileNumberCheck"));
	
	@FindBy(id="mobileNumberCheck") 
	private WebElement username;
	
	@FindBy(name="TxtPWD") 
	private WebElement password;
	
	@FindBy(xpath="//input[@id='BtnLogin']")
	private WebElement loginbutton;
	
	@FindBy(xpath="//input[@id='user']")
	private WebElement smsuser;
	
	@FindBy(xpath="//input[@id='pass']")
	private WebElement smspass;
	
	@FindBy(xpath="//input[@value='Login']")
	private WebElement smsLoginbutton;
	
//	driver.findElement(By.xpath("//input[@id='user']")).sendKeys("paynearby");
//	//Pnb@123
//	driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("Pnb@123");
//	driver.findElement(By.xpath("//input[@value='Login']")).click();
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void enterUsername()
	{
		username.clear();
		username.sendKeys("7021530275");
	}
	
	public void enterPassword()
	{
		password.sendKeys("Admin@123");
	}
	
	public void clickOnLoginButton()
	{
		loginbutton.click();
	}
	
	public void entersmsUsername()
	{
		smsuser.sendKeys("Paynearby");
	}
	
	public void entersmsPassword()
	{
		smspass.sendKeys("Pnb@123");
	}
	
	public void clickonSMSLoginButton()
	{
		smsLoginbutton.click();
	}
	
	
	

}
