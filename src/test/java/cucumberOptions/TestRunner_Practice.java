package cucumberOptions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/PaynearbyLogin.feature", glue="stepDefinitions", 
stepNotifications=true,  tags= "@Smoke",
plugin= {"json:target/cucumber.json",
		"pretty","html:target/htmlReports/cucumber.html"})

public class TestRunner_Practice {

}
