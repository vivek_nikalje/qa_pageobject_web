package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Login_StepDefinitions {
	WebDriver driver;
	
	@Given("landing to web retailer url")
	public void landing_to_web_retailer_url() {
		
		System.out.println("landing to web retailer url");
		System.setProperty("webdriver.chrome.driver", "F:\\AutomationWithMohan\\chromedriver_win32 (10)\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://retailer.paynearby.in");
	   
	}

	@When("user login with username and password")
	public void user_login_with_username_and_password() throws InterruptedException {
		System.out.println("user login with username and password");
		Thread.sleep(5000);
		// enter username
		driver.findElement(By.id("mobileNumberCheck")).sendKeys("7021530275");
		//enter password
		
		driver.findElement(By.name("TxtPWD")).sendKeys("Admin@123");
		//click on login button
	
		driver.findElement(By.xpath("//input[@id='BtnLogin']")).click();
	}

	@Then("Home page should display")
	public void home_page_should_display() {
		System.out.println("Home page should display");
	}

	@Then("username is displayed on homepage")
	public void username_is_displayed_on_homepage() {
		System.out.println("username is displayed on homepage");
	}
	
	@When("user login with invalid username and password")
	public void user_login_with_invalid_username_and_password() throws InterruptedException {
		
		Thread.sleep(5000);
		// enter username
		driver.findElement(By.id("mobileNumberCheck")).sendKeys("7021530275");
		//enter password
		
		Thread.sleep(2000);
		driver.findElement(By.name("TxtPWD")).sendKeys("133");
		//click on login button
	
		driver.findElement(By.xpath("//input[@id='BtnLogin']")).click();
	   
	}
	
	@Then("Error message should display")
	public void error_message_should_display() {
	  
		WebElement errormessage=driver.findElement(By.xpath("//span[text()='Invalid UID / Password']"));
		if(errormessage.isDisplayed())
		{
			System.out.println(errormessage.getText());
		}
		else
		{
			System.out.println("error message not found");
			
		}
		System.out.println("Test complete");
		System.out.println("Test completed again");
		System.out.println("Test completed again");
	}

}
