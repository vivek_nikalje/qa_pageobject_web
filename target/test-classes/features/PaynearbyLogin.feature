Feature: Test Login functionality
@Smoke
Scenario: Verify Login functionality with valid details

Given landing to web retailer url
When user login with username and password
Then Home page should display
And username is displayed on homepage
@Smoke
Scenario: Verify Login functionality with invalid details

Given landing to web retailer url
When user login with invalid username and password
Then Error message should display


